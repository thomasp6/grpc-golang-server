gRPC server implementation in Golang
---- 

First install dependencies with `go mod tidy`.

Compile your proto files with `protoc` thanks to the following command: 
```sh
protoc --go_out=. --go_opt=paths=source_relative \
    --go-grpc_out=. --go-grpc_opt=paths=source_relative \
    project/project.proto
```

It will output them in the `project` folder.
