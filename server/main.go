package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"
	"time"

	pb "project/project"

	"google.golang.org/grpc"
)

var (
	port = flag.Int("port", 50051, "The server port")
)

// server is used to implement helloworld.GreeterServer.
type server struct {
	pb.ProjectServer
}

func (s *server) CreateProject(ctx context.Context, in *pb.CreateProjectRequest) (*pb.ProjectResponse, error) {
	log.Printf("Received: %v", in)
	return &pb.ProjectResponse{
		ProjectId:   "project_id",
		Name:        in.GetName(),
		Environment: in.GetEnvironment(),
		Suspended:   false,
		Flags:       in.GetFlags(),
		CreatedAt:   time.Now().String(),
	}, nil
}

func main() {
	flag.Parse()
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterProjectServer(s, &server{})
	log.Printf("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
